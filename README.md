# Final Praktikum Sistem Operasi

Anggota Kelompok ``B11`` 
| Nama                      | NRP        |
|---------------------------|------------|
| Rayhan Arvianta Bayuputra | 5025211217 |
| Danno Denis Dhaifullah    | 5025211027 |
| Samuel Berkat Hulu        | 5025201055 |

Almas D. Afhin adalah seseorang yang bercita-cita menjadi raja bajak laut. Dia tinggal di Desa Keputih. Pamannya, Sua D. Wahyu adalah seorang Infrastructure Engineer yang bekerja untuk Revolutionary Army. Namun, pada suatu hari, World Government yang bermusuhan dengan Revolutionary Army melancarkan Zero-Day attack pada program server Revolutionary Army yang mengakibatkan website Revolutionary Army menjadi tidak bisa diakses.

Karena Ayah Almas jugalah seorang hacker dan maintainer handal, dia berusaha untuk memperbaiki exploit dari server tersebut. Namun, website Revolutionary Army haruslah tetap berjalan, maka dia meminta bantuan Almas untuk membuat server backup sementara program server utama masih dalam perbaikan.

Karena ini merupakan tugas yang cukup susah, Almas meminta bantuan temannya yaitu Gloriyano D. Aniel. Kalian di sini berperan sebagai Gloriyano yang akan membantu Almas dalam membuat server.

## Sistem Database Sederhana

**Bagaimana Program Diakses**

- Program server berjalan sebagai **daemon**.
- Untuk bisa akses console database, perlu buka program _client_ (kalau di Linux seperti _command_ **mysql** di **bash**).
- Program _client_ dan utama berinteraksi lewat socket.
- Program _client_ bisa mengakses server dari mana saja.

**Struktur Direktori**

Struktur repository (di repository Gitlab)
```
database/
[program_database].c
[settingan_cron_backup]
client/
[program_client].c
dump/
[program_dump_client].c
```

**Penamaan dari program bebas**

```
Struktur server (ketika program dijalankan)

[folder_server_database]
[program_database]
database/
[nama_database]/ → Directory
[nama_tabel] → File
```

**Contoh struktur untuk server**

```
database/
program_databaseku
databases/
database1/
table11
table12
database2/
table21
table22
table23
```

**Bagaimana struktur data saat tersimpan di dalam _file_ tabel bebas**

## Bagaimana Database Digunakan

**A. Autentikasi**

- Terdapat _user_ dengan **_username_** dan **_password_**  yang digunakan untuk mengakses _database_ yang merupakan haknya (_database_ yang memiliki akses dengan _username_ tersebut). Namun, jika _user_ merupakan **root (sudo)**, maka bisa mengakses semua _database_ yang ada.

**Catatan**: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.

```
Format
./[program_client_database] -u [username] -p [password]
```

```
Contoh
./client_databaseku -u khonsu -p khonsu123
```
- **_Username, password_, dan hak akses _database_** disimpan di suatu database juga, tapi tidak ada _user_ yang bisa akses _database_ tersebut kecuali mengakses menggunakan **_root_**.

- _User_ **root** sudah ada dari awal.

```
Contoh cara user root mengakses program client

sudo ./client_database

```

- Menambahkan _user_ (Hanya bisa dilakukan user root)

```
Format
CREATE USER [nama_user] IDENTIFIED BY [password_user];
```

```
Contoh
CREATE USER khonsu IDENTIFIED BY khonsu123;

```


**B. Autorisasi**

- Untuk dapat mengakses database yang dia punya, _permission_ dilakukan dengan _command_. Pembuatan tabel dan semua DML butuh untuk mengakses _database_ terlebih dahulu.

```
Format
USE [nama_database];
```

```
Contoh
USE database1;
```



- Yang bisa memberikan _permission_ atas _database_ untuk suatu _user_ hanya **root**.

```
Format
GRANT PERMISSION [nama_database] INTO [nama_user];
```

- _User_ hanya bisa mengakses _database_ di mana dia diberi _permission_ untuk _database_ tersebut


**C. Data Definition Language**

- _Input_ penamaan _database_, tabel, dan kolom hanya angka dan huruf.

- Semua _user_ bisa membuat _database_, otomatis _user_ tersebut memiliki _permission_ untuk _database_ tersebut.

```
Format
CREATE DATABASE [nama_database];
```

```
Contoh
CREATE DATABASE database1;
```

- **Root** dan _user_ yang memiliki _permission_ untuk suatu _database_ untuk bisa membuat tabel untuk _database_ tersebut, tentunya setelah mengakses _database_ tersebut. Tipe data dari semua kolom adalah **_string_** atau **_integer_**. Jumlah kolom bebas.
```
Format
CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);
```

```
Contoh
CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);
```

- Bisa melakukan **DROP database**, _table_ (setelah mengakses database), dan kolom. Jika sasaran _drop_ ada maka di-_drop_, jika tidak ada maka biarkan.

```
Format
DROP [DATABASE | TABLE | COLUMN] [nama_database | nama_tabel | [nama_kolom] FROM [nama_tabel]];
```

```
Contoh

Drop database
DROP DATABASE database1;

Drop table
DROP TABLE table1;

Drop Column
DROP COLUMN kolom1 FROM table1;
```


**D. Data Manipulation Language**

- **INSERT**

Hanya bisa _insert_ satu row per satu _command_. _Insert_ sesuai dengan jumlah dan urutan kolom.

```
Format
INSERT INTO [nama_tabel] ([value], ...);
```

```
Contoh
INSERT INTO table1 (‘value1’, 2, ‘value3’, 4);
```

- **UPDATE**

Hanya bisa _update_ satu kolom per satu _command_.

```
Format
UPDATE [nama_tabel] SET [nama_kolom]=[value];
```

```
Contoh
UPDATE table1 SET kolom1=’new_value1’;
```

- **DELETE**

_Delete_ data yang ada di tabel.

```
Format
DELETE FROM [nama_tabel];
```

```
Contoh
DELETE FROM table1;
```

- **SELECT**
```
Format
SELECT [nama_kolom, … | *] FROM [nama_tabel];
```

```
Contoh 1
SELECT kolom1, kolom2 FROM table1;
```
```
Contoh 2
SELECT * FROM table1;
```

- **WHERE**

_Command_ **UPDATE, SELECT**, dan **DELETE** bisa dikombinasikan dengan **WHERE. WHERE** hanya untuk satu kondisi. Dan hanya ‘=’.
```
Format
[Command UPDATE, SELECT, DELETE] WHERE [nama_kolom]=[value];
```

```
Contoh
DELETE FROM table1 WHERE kolom1=’value1’;
```

**E. Logging**

- Setiap _command_ yang dipakai harus dilakukan _logging_ ke suatu _file_ dengan format. Jika yang eksekusi **root**, maka _username_ **root**.

```
Format di dalam log
timestamp(yyyy-mm-dd hh:mm:ss):username:command
```

```
Contoh
2021-05-19 02:05:15:khonsu:SELECT FROM table1
```

**F. Reliability**

- Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel.

```
Format
./[program_dump_database] -u [username] -p [password] [nama_database]
```

```
Contoh
./databasedump -u khonsu -p khonsu123 database1 > database1.backup
```

Contoh hasil isi file database1.backup adalah sebagai berikut.

```
DROP TABLE table1;
CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);

DROP TABLE table2;
CREATE TABLE table2 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
```

- Program _dump database_ **dijalankan tiap jam** untuk semua _database_ dan _log_, lalu di **zip** sesuai _timestamp_, lalu _log_ dikosongkan kembali.

**G. Tambahan**

- Kita bisa memasukkan _command_ lewat _file_ dengan _redirection_ di program _client_.

```
Format
./[program_client_database] -u [username] -p [password] -d [database] < [file_command]
```

```
Contoh
./client_databaseku -u khonsu -p khonsu123 -d database1 < database.backup
```

**H. Error Handling**

- Jika ada _command_ yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan _error_ **tanpa keluar dari program client.**

**I. Containerization**

- Buatlah **Dockerfile** yang berisi semua langkah yang diperlukan untuk setup _environment_ dan menentukan bagaimana aplikasi harus dijalankan. Setelah **Dockerfile** berhasil dibuat, langkah selanjutnya adalah membuat **Docker Image**. Gunakan **Docker CLI** untuk mem-_build image_ dari **Dockerfile** kalian. Setelah berhasil membuat _image_, verifikasi bahwa _image_ tersebut berfungsi seperti yang diharapkan dengan menjalankan **Docker Container** dan memeriksa _output_-nya.

- _Publish_ **Docker Image** sistem ke **Docker Hub**. _Output_ dari pekerjaan ini adalah _file_ Docker kalian bisa dilihat secara _public_ pada https://hub.docker.com/r/{Username}/storage-app.

- Untuk memastikan sistem kalian mampu menangani peningkatan penggunaan, kalian memutuskan untuk menerapkan skala pada layanan menggunakan **Docker Compose** dengan **_instance_ sebanyak 5**. Buat _folder_ terpisah bernama **Sukolilo, Keputih, Gebang, Mulyos, dan Semolowaru** dan jalankan **Docker Compose** di sana.

**J. Extra**

- Program _client_ dan _dump_ berkomunikasi **dengan socket** ke program utama dan **tidak boleh** langsung mengakses _file-file_ di folder program utama.

- _Requirement_ di sini adalah _requirement_ minimum, jika Anda mengerjakan melebihi _requirement_ di sini, maka akan mendapatkan nilai tambah yang bisa digunakan jika ada nilai dari modul sebelumnya yang belum maksimal.

- Terapkan apa saja yang sudah kalian pelajari di praktikum Sistem Operasi ini untuk mengerjakan _final project_.

## PENYELESAIAN

### client.c

```c
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4012

struct Permission {
  char name[10000];
  char pass[10000];
};

int checkPermission(char *username, char *password) {
  FILE *file;
  struct Permission client;
  int id, flag = 0;
  file = fopen("../database/databases/user.dat", "rb");

  while (fread(&client, sizeof(client), 1, file)) {
    if (strcmp(client.name, username) == 0 && strcmp(client.pass, password) == 0) {
      flag = 1;
      break;
    }
  }

  fclose(file);

  if (flag == 0) {
    printf("Not Allowed: No permission\n");
    return 0;
  } else {
    return 1;
  }
}

void writeLog(char *command, char *name) {
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char log_entry[1000];

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof(loc), "../database/log/log%s.log", name);
  file = fopen(loc, "ab");

  sprintf(log_entry, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, command);

  fputs(log_entry, file);
  fclose(file);
}

int main(int argc, char *argv[]) {
  int allowed = 0;
  int user_id = geteuid();
  char used_db[1000];

  if (geteuid() == 0)
    allowed = 1;
  else
    allowed = checkPermission(argv[2], argv[4]);

  if (allowed == 0)
    return 0;

  int client_socket, ret;
  struct sockaddr_in server_addr;
  char buffer[32000];

  client_socket = socket(AF_INET, SOCK_STREAM, 0);

  if (client_socket < 0) {
    printf("[-]Error creating socket.\n");
    exit(1);
  }

  printf("[+]Client Socket created.\n");

  memset(&server_addr, '\0', sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(PORT);
  server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));

  if (ret < 0) {
    printf("[-]Error connecting.\n");
    exit(1);
  }
  printf("[+]Connected to the server.\n");

  while (1) {
    printf("Client: \t");
    char input[10000];
    char temp[10000];
    char cmd[100][10000];
    char *token;
    int i = 0;
    scanf(" %[^\n]s", input);
    strcpy(temp, input);
    token = strtok(input, " ");

    while (token != NULL) {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, " ");
    }

    int false_cmd = 0;
    if (strcmp(cmd[0], "CREATE") == 0) {
      if (strcmp(cmd[1], "USER") == 0 && strcmp(cmd[3], "IDENTIFIED") == 0 && strcmp(cmd[4], "BY") == 0) {
        snprintf(buffer, sizeof(buffer), "cUser:%s:%s:%d", cmd[2], cmd[5], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "cDatabase:%s:%s:%d", cmd[2], argv[2], user_id);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "cTable:%s", temp);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    else if (strcmp(cmd[0], "GRANT") == 0 && strcmp(cmd[1], "PERMISSION") == 0 && strcmp(cmd[3], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "gPermission:%s:%s:%d", cmd[2], cmd[4], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "USE") == 0) {
      snprintf(buffer, sizeof(buffer), "uDatabase:%s:%s:%d", cmd[1], argv[2], user_id);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "cekCurrentDatabase") == 0) {
      snprintf(buffer, sizeof(buffer), "%s", cmd[0]);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "DROP") == 0) {
      if (strcmp(cmd[1], "DATABASE") == 0) {
        snprintf(buffer, sizeof(buffer), "dDatabase:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "TABLE") == 0) {
        snprintf(buffer, sizeof(buffer), "dTable:%s:%s", cmd[2], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
      else if (strcmp(cmd[1], "COLUMN") == 0) {
        snprintf(buffer, sizeof(buffer), "dColumn:%s:%s:%s", cmd[2], cmd[4], argv[2]);
        send(client_socket, buffer, strlen(buffer), 0);
      }
    }
    else if (strcmp(cmd[0], "INSERT") == 0 && strcmp(cmd[1], "INTO") == 0) {
      snprintf(buffer, sizeof(buffer), "insert:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "UPDATE") == 0) {
      snprintf(buffer, sizeof(buffer), "update:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "DELETE") == 0) {
      snprintf(buffer, sizeof(buffer), "delete:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], "SELECT") == 0) {
      snprintf(buffer, sizeof(buffer), "select:%s", temp);
      send(client_socket, buffer, strlen(buffer), 0);
    }
    else if (strcmp(cmd[0], ":exit") != 0) {
      false_cmd = 1;
      char warning[] = "Invalid Command";
      send(client_socket, warning, strlen(warning), 0);
    }

    if (false_cmd != 1) {
      char sender[10000];
      if (user_id == 0)
        strcpy(sender, "root");
      else
        strcpy(sender, argv[2]);
      writeLog(temp, sender);
    }

    if (strcmp(cmd[0], ":exit") == 0) {
      send(client_socket, cmd[0], strlen(cmd[0]), 0);
      close(client_socket);
      printf("[-]Disconnected from server.\n");
      exit(1);
    }
    bzero(buffer, sizeof(buffer));
    if (recv(client_socket, buffer, 1024, 0) < 0)
      printf("[-]Error receiving data.\n");
    else
      printf("Server: \t%s\n", buffer);
  }

  return 0;
}
```

**Penjelasan:**

```c
struct Permission {
  char name[10000];
  char pass[10000];
};
```

- **Pertama**, deklarasi struct dengan nama ``Permission`` yang memiliki dua variabel, ``name`` dan ``pass``.

```c
int checkPermission(char *username, char *password){ ... }
```

- **Kedua**, fungsi ``checkPermission`` digunakan untuk memeriksa izin akses _user_ berdasarkan username dan password yang diberikan. Fungsi ini mengambil dua argumen, yaitu ``username`` dan ``password``, yang merupakan string yang akan digunakan untuk memverifikasi izin akses _user_.


```c
void writeLog(char *command, char *name){ ... }
```

- **Ketiga**, fungsi writeLog adalah sebuah fungsi yang digunakan untuk mencatat aktivitas _user_ ke dalam file log. Fungsi ini memiliki dua parameter, yaitu command dan name, yang merupakan string yang akan dicatat dalam log.

Lalu masuk ke bagian ``main``.

```c
int allowed = 0;
int user_id = geteuid();
char used_db[1000];
```

- `allowed` digunakan untuk menyimpan status izin akses _user_, yang kemungkinan akan diubah nilainya dalam bagian kode selanjutnya.

- `user_id` digunakan untuk melakukan operasi atau pengambilan keputusan yang terkait dengan hak akses _user_.

- `used_db` digunakan untuk menyimpan nama atau _path database_ yang sedang digunakan atau akan digunakan dalam kode selanjutnya.


```c
if (geteuid() == 0)
  allowed = 1;
else
  allowed = checkPermission(argv[2], argv[4]);

if (allowed == 0)
  return 0;
```

- Izin akses _user_ diuji dan variabel allowed akan memiliki nilai 1 jika _user_ adalah root atau jika izin _user_ telah berhasil diverifikasi menggunakan fungsi checkPermission. Jika allowed adalah 0, maka _user_ tidak memiliki izin akses yang diperlukan dan kode mungkin akan mengambil tindakan sesuai dengan keputusan tersebut.

```c
int client_socket, ret;
struct sockaddr_in server_addr;
char buffer[32000];

client_socket = socket(AF_INET, SOCK_STREAM, 0);
```

- Soket klien baru dibuat dengan menggunakan protokol `TCP` pada domain `IPv4`. Soket tersebut akan digunakan untuk mengirim dan menerima data melalui koneksi TCP dengan server yang akan ditentukan selanjutnya.

```c
if (client_socket < 0) {
  printf("[-]Error creating socket.\n");
  exit(1);
}

printf("[+]Client Socket created.\n");

memset(&server_addr, '\0', sizeof(server_addr));
server_addr.sin_family = AF_INET;
server_addr.sin_port = htons(PORT);
server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");

ret = connect(client_socket, (struct sockaddr *)&server_addr, sizeof(server_addr));

if (ret < 0) {
  printf("[-]Error connecting.\n");
  exit(1);
}

printf("[+]Connected to the server.\n");
```

- **if (client_socket < 0) { ... }** memeriksa apakah pembuatan soket berhasil. Jika pembuatan soket berhasil, maka pesan "[+]Client Socket created." akan dicetak.

- **memset(&server_addr, '\0', sizeof(server_addr));** digunakan untuk mengisi struktur `server_addr` dengan nol (null) sebelum menginisialisasi nilainya.

- **server_addr.sin_family = AF_INET;** mengatur jenis alamat yang digunakan oleh soket.

- **server_addr.sin_port = htons(PORT);** mengatur nomor port server yang akan dikoneksi oleh klien.

- **server_addr.sin_addr.s_addr = inet_addr("127.0.0.1");** mengatur alamat IP server yang akan dikoneksi oleh klien, yaitu "127.0.0.1".

- **ret = connect( ... );** adalah pemanggilan fungsi `connect()` yang digunakan untuk melakukan koneksi ke server. Nilai _return_ dari fungsi `connect()` akan disimpan dalam ret.

```c
while (1) { ... }
```

- Di dalam loop tersebut, klien menerima input dari _user_, memproses input tersebut, mengirimkannya ke server, menerima respons dari server, dan menampilkan respons tersebut ke _user_.


### database.c

```c
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 4012

struct table
{
  int tot_column;
  char type[100][10000];
  char data[100][10000];
};

struct permission
{
  char name[10000];
  char pass[10000];
};

struct permission_db
{
  char database[10000];
  char name[10000];
};

int isUserExist(char *uname)
{
  FILE *file;
  struct permission user;

  int id, mark = 0;

  file = fopen("../database/databases/user.dat", "rb");
  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, uname) == 0)
      return 1;
    if (feof(file))
      break;
  }
  fclose(file);
  return 0;
}

void createUser(char *name, char *pass)
{
  struct permission user;
  strcpy(user.name, name);
  strcpy(user.pass, pass);

  printf("%s %s\n", user.name, user.pass);

  char fname[] = {"databases/user.dat"};

  FILE *file;
  file = fopen(fname, "ab");
  fwrite(&user, sizeof(user), 1, file);
  fclose(file);
}

int isAllowedDb(char *name, char *database)
{
  FILE *file;
  struct permission_db user;
  int id, mark = 0;
  printf("name = %s  database = %s", name, database);
  file = fopen("../database/databases/permission.dat", "rb");
  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, name) == 0)
    {
      if (strcmp(user.database, database) == 0)
        return 1;
    }
    if (feof(file))
      break;
  }
  fclose(file);
  return 0;
}

void insertPermission(char *name, char *database)
{
  struct permission_db user;

  strcpy(user.name, name);
  strcpy(user.database, database);

  printf("%s %s\n", user.name, user.database);

  char fname[] = {"databases/permission.dat"};
  FILE *file;
  file = fopen(fname, "ab");
  fwrite(&user, sizeof(user), 1, file);
  fclose(file);
}

int findColumn(char *table, char *column)
{
  FILE *file;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  fread(&user, sizeof(user), 1, file);
  int index = -1;

  for (int i = 0; i < user.tot_column; i++)
  {
    if (strcmp(user.data[i], column) == 0)
      index = i;
  }

  if (feof(file))
    return -1;

  fclose(file);
  return index;
}

int deleteColumn(char *table, int index)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "wb");

  while (1)
  {
    fread(&user, sizeof(user), 1, file);

    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index)
        continue;

      strcpy(temp_user.data[iteration], user.data[i]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }
    temp_user.tot_column = user.tot_column - 1;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
  }

  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int deleteTable(char *table, char *namaTable)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  fread(&user, sizeof(user), 1, file);
  int index = -1;
  struct table temp_user;

  for (int i = 0; i < user.tot_column; i++)
  {
    strcpy(temp_user.data[i], user.data[i]);
    strcpy(temp_user.type[i], user.type[i]);
  }

  temp_user.tot_column = user.tot_column;
  fwrite(&temp_user, sizeof(temp_user), 1, file1);
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 1;
}

int updateColumn(char *table, int index, char *ganti)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;
    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0)
        strcpy(temp_user.data[iteration], ganti);
      else
        strcpy(temp_user.data[iteration], user.data[i]);

      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }
    temp_user.tot_column = user.tot_column;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int updateColumnWhere(char *table, int index, char *ganti, int change_index, char *where)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0 && strcmp(user.data[change_index], where) == 0)
        strcpy(temp_user.data[iteration], ganti);
      else
        strcpy(temp_user.data[iteration], user.data[i]);
      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }

    temp_user.tot_column = user.tot_column;
    fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

int deleteTableWhere(char *table, int index, char *column, char *where)
{
  FILE *file, *file1;
  struct table user;
  int id, mark = 0;
  file = fopen(table, "rb");
  file1 = fopen("temp", "ab");
  int data_take = 0;

  while (1)
  {
    mark = 0;
    fread(&user, sizeof(user), 1, file);

    if (feof(file))
      break;

    struct table temp_user;
    int iteration = 0;

    for (int i = 0; i < user.tot_column; i++)
    {
      if (i == index && data_take != 0 && strcmp(user.data[i], where) == 0)
        mark = 1;
      strcpy(temp_user.data[iteration], user.data[i]);
      printf("%s\n", temp_user.data[iteration]);
      strcpy(temp_user.type[iteration], user.type[i]);
      printf("%s\n", temp_user.data[iteration]);
      iteration++;
    }

    temp_user.tot_column = user.tot_column;
    if (mark != 1)
      fwrite(&temp_user, sizeof(temp_user), 1, file1);
    data_take++;
  }
  fclose(file);
  fclose(file1);
  remove(table);
  rename("temp", table);
  return 0;
}

void writelog(char *cmd, char *name)
{
  time_t times;
  struct tm *info;
  time(&times);
  info = localtime(&times);

  char info_log[1000];

  FILE *file;
  file = fopen("logUser.log", "ab");

  sprintf(info_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n", info->tm_year + 1900, info->tm_mon + 1, info->tm_mday, info->tm_hour, info->tm_min, info->tm_sec, name, cmd);

  fputs(info_log, file);
  fclose(file);
  return;
}

int main()
{

  int sockfd, ret;
  struct sockaddr_in serverAddr;

  int new_socket;
  struct sockaddr_in newAddr;

  socklen_t addr_size;

  char buff[1024];
  pid_t childpid;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd < 0)
  {
    printf("[-]Error connection.\n");
    exit(1);
  }
  printf("[+]Socket created.\n");

  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(PORT);
  serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

  ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  if (ret < 0)
  {
    printf("[-]Error in binding.\n");
    exit(1);
  }
  printf("[+]Bind to port %d\n", PORT);

  if (listen(sockfd, 10) == 0)
    printf("[+]Listening....\n");
  else
    printf("[-]Error in binding.\n");

  while (1)
  {
    new_socket = accept(sockfd, (struct sockaddr *)&newAddr, &addr_size);

    if (new_socket < 0)
      exit(1);
    printf("Connection accepted from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));

    if ((childpid = fork()) == 0)
    {
      close(sockfd);

      while (1)
      {
        recv(new_socket, buff, 1024, 0);
        char *token;
        char temp_buff[32000];
        strcpy(temp_buff, buff);
        char cmd[100][10000];
        token = strtok(temp_buff, ":");
        int i = 0;
        char used_db[1000];

        while (token != NULL)
        {
          strcpy(cmd[i], token);
          i++;
          token = strtok(NULL, ":");
        }
        if (strcmp(cmd[0], "cUser") == 0)
        {
          if (strcmp(cmd[3], "0") == 0)
            createUser(cmd[1], cmd[2]);
          else
          {
            char warning[] = "Not Allowed: Not have permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "gPermission") == 0)
        {
          if (strcmp(cmd[3], "0") == 0)
          {
            int exist = isUserExist(cmd[2]);
            if (exist == 1)
              insertPermission(cmd[2], cmd[1]);
            else
            {
              char warning[] = "User Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
          }
          else
          {
            char warning[] = "Not Allowed: Not have permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "cDatabase") == 0)
        {
          char loc[20000];
          snprintf(loc, sizeof loc, "databases/%s", cmd[1]);
          printf("location = %s, name = %s , database = %s\n", loc, cmd[2], cmd[1]);
          mkdir(loc, 0777);
          insertPermission(cmd[2], cmd[1]);
        }
        else if (strcmp(cmd[0], "uDatabase") == 0)
        {
          if (strcmp(cmd[3], "0") != 0)
          {
            int allowed = isAllowedDb(cmd[2], cmd[1]);
            if (allowed != 1)
            {
              char warning[] = "Access_database : You're Not Allowed";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
            else
            {
              strncpy(used_db, cmd[1], sizeof(cmd[1]));
              char warning[] = "Access_database : Allowed";
              printf("used_db = %s\n", used_db);
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
            }
          }
        }
        else if (strcmp(cmd[0], "cekCurrentDatabase") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
          }
          send(new_socket, used_db, strlen(used_db), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "cTable") == 0)
        {
          printf("%s\n", cmd[1]);
          char *toks;

          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
          }
          else
          {
            char query_list[100][10000];
            char temp_cmd[20000];
            snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
            toks = strtok(temp_cmd, "(), ");
            int total = 0;

            while (toks != NULL)
            {
              strcpy(query_list[total], toks);
              printf("%s\n", query_list[total]);
              total++;
              toks = strtok(NULL, "(), ");
            }

            char create_table[20000];
            snprintf(create_table, sizeof create_table, "../database/databases/%s/%s", used_db, query_list[2]);
            int iteration = 0;
            int data_iteration = 3;
            struct table column;

            while (total > 3)
            {
              strcpy(column.data[iteration], query_list[data_iteration]);
              printf("%s\n", column.data[iteration]);
              strcpy(column.type[iteration], query_list[data_iteration + 1]);
              data_iteration = data_iteration + 2;
              total = total - 2;
              iteration++;
            }

            column.tot_column = iteration;
            printf("iteration = %d\n", iteration);
            FILE *file;
            printf("%s\n", create_table);
            file = fopen(create_table, "ab");
            fwrite(&column, sizeof(column), 1, file);
            fclose(file);
          }
        }
        else if (strcmp(cmd[0], "dDatabase") == 0)
        {
          int allowed = isAllowedDb(cmd[2], cmd[1]);

          if (allowed != 1)
          {
            char warning[] = "Access_database : Not Allowed, no permission";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          else
          {
            char delete[20000];
            snprintf(delete, sizeof delete, "rm -r databases/%s", cmd[1]);
            system(delete);
            char warning[] = "Database Has Been Removed";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
          }
        }
        else if (strcmp(cmd[0], "dTable") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char delete[20000];
          snprintf(delete, sizeof delete, "databases/%s/%s", used_db, cmd[1]);
          remove(delete);
          char warning[] = "Table Has Been Removed";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "dColumn") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, cmd[2]);
          int index = findColumn(create_table, cmd[1]);

          if (index == -1)
          {
            char warning[] = "Column Not Found";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          deleteColumn(create_table, index);
          char warning[] = "Column Has Been Removed";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "insert") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(), ");
          int total = 0;

          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            total++;
            toks = strtok(NULL, "\'(), ");
          }

          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
          FILE *file;
          int total_column;
          file = fopen(create_table, "r");

          if (file == NULL)
          {
            char warning[] = "TABLE NOT FOUND";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          else
          {
            struct table user;
            fread(&user, sizeof(user), 1, file);
            total_column = user.tot_column;
            fclose(file);
          }

          int iteration = 0;
          int data_iteration = 3;
          struct table column;

          while (total > 3)
          {
            strcpy(column.data[iteration], query_list[data_iteration]);
            printf("%s\n", column.data[iteration]);
            strcpy(column.type[iteration], "string");
            data_iteration++;
            total = total - 1;
            iteration++;
          }

          column.tot_column = iteration;
          if (total_column != column.tot_column)
          {
            char warning[] = "YOUR INPUT NOT MATCH THE COLUMN";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }

          printf("iteration = %d\n", iteration);
          FILE *file1;
          printf("%s\n", create_table);
          file1 = fopen(create_table, "ab");
          fwrite(&column, sizeof(column), 1, file1);
          fclose(file1);
          char warning[] = "Data Has Been Inserted";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "update") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("total = %d\n", total);
          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[1]);
          if (total == 5)
          {
            printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
            int index = findColumn(create_table, query_list[3]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("index = %d\n", index);
            updateColumn(create_table, index, query_list[4]);
          }
          else if (total == 8)
          {
            printf("buat table = %s, kolumn = %s", create_table, query_list[3]);
            int index = findColumn(create_table, query_list[3]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("%s\n", query_list[7]);
            int change_index = findColumn(create_table, query_list[6]);
            updateColumnWhere(create_table, index, query_list[4], change_index, query_list[7]);
          }
          else
          {
            char warning[] = "Data Has Been Deleted";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char warning[] = "Data Has Been Updated";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "delete") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("total = %d\n", total);
          char create_table[20000];
          snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[2]);
          if (total == 3)
          {
            deleteTable(create_table, query_list[2]);
          }
          else if (total == 6)
          {
            int index = findColumn(create_table, query_list[4]);
            if (index == -1)
            {
              char warning[] = "Column Not Found";
              send(new_socket, warning, strlen(warning), 0);
              bzero(buff, sizeof(buff));
              continue;
            }
            printf("index  = %d\n", index);
            deleteTableWhere(create_table, index, query_list[4], query_list[5]);
          }
          else
          {
            char warning[] = "Wrong input";
            send(new_socket, warning, strlen(warning), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char warning[] = "Data Has Been Deleted";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        else if (strcmp(cmd[0], "select") == 0)
        {
          if (used_db[0] == '\0')
          {
            strcpy(used_db, "You're not selecting database yet");
            send(new_socket, used_db, strlen(used_db), 0);
            bzero(buff, sizeof(buff));
            continue;
          }
          char query_list[100][10000];
          char temp_cmd[20000];
          snprintf(temp_cmd, sizeof temp_cmd, "%s", cmd[1]);
          char *toks;
          toks = strtok(temp_cmd, "\'(),= ");
          int total = 0;
          while (toks != NULL)
          {
            strcpy(query_list[total], toks);
            printf("%s\n", query_list[total]);
            total++;
            toks = strtok(NULL, "\'(),= ");
          }
          printf("ABC\n");
          if (total == 4)
          {
            char create_table[20000];
            snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
            printf("buat table = %s", create_table);
            char perintahKolom[1000];
            printf("masuk 4\n");
            if (strcmp(query_list[1], "*") == 0)
            {
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  char padding[20000];
                  snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                  strcat(buffers, padding);
                }
                strcat(sendDatabase, buffers);
              }
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
              fclose(file);
            }
            else
            {
              int index = findColumn(create_table, query_list[1]);
              printf("%d\n", index);
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (i == index)
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              printf("ini send fix\n%s\n", sendDatabase);
              fclose(file);
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
            }
          }
          else if (total == 7 && strcmp(query_list[4], "WHERE") == 0)
          {
            char create_table[20000];
            snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[3]);
            printf("buat table = %s", create_table);
            char perintahKolom[1000];
            printf("masuk 4\n");
            if (strcmp(query_list[1], "*") == 0)
            {
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              int index = findColumn(create_table, query_list[5]);
              printf("%d\n", index);
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (strcmp(user.data[index], query_list[6]) == 0)
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
              fclose(file);
            }
            else
            {
              int index = findColumn(create_table, query_list[1]);
              printf("%d\n", index);
              FILE *file, *file1;
              struct table user;
              int id, mark = 0;
              int change_index = findColumn(create_table, query_list[5]);
              file = fopen(create_table, "rb");
              char buffers[40000];
              char sendDatabase[40000];
              bzero(buff, sizeof(buff));
              bzero(sendDatabase, sizeof(sendDatabase));
              while (1)
              {
                char enter[] = "\n";
                fread(&user, sizeof(user), 1, file);
                snprintf(buffers, sizeof buffers, "\n");
                if (feof(file))
                {
                  break;
                }
                for (int i = 0; i < user.tot_column; i++)
                {
                  if (i == index && (strcmp(user.data[change_index], query_list[6]) == 0 || strcmp(user.data[i], query_list[5]) == 0))
                  {
                    char padding[20000];
                    snprintf(padding, sizeof padding, "%s\t", user.data[i]);
                    strcat(buffers, padding);
                  }
                }
                strcat(sendDatabase, buffers);
              }
              printf("ini send fix\n%s\n", sendDatabase);
              fclose(file);
              send(new_socket, sendDatabase, strlen(sendDatabase), 0);
              bzero(sendDatabase, sizeof(sendDatabase));
              bzero(buff, sizeof(buff));
            }
          }
          else
          {
            printf("ini query 3 %s", query_list[total - 3]);
            if (strcmp(query_list[total - 3], "WHERE") != 0)
            {
              char create_table[20000];
              snprintf(create_table, sizeof create_table, "databases/%s/%s", used_db, query_list[total - 1]);
              printf("buat table = %s", create_table);
              printf("tanpa where");
              int index[100];
              int iteration = 0;
              for (int i = 1; i < total - 2; i++)
              {
                index[iteration] = findColumn(create_table, query_list[i]);
                printf("%d\n", index[iteration]);
                iteration++;
              }
            }
            else if (strcmp(query_list[total - 3], "WHERE") == 0)
            {
              printf("dengan where");
            }
          }
        }
        else if (strcmp(cmd[0], "log") == 0)
        {
          writelog(cmd[1], cmd[2]);
          char warning[] = "\n";
          send(new_socket, warning, strlen(warning), 0);
          bzero(buff, sizeof(buff));
        }
        if (strcmp(buff, ":exit") == 0)
        {
          printf("Disconnected from %s:%d\n", inet_ntoa(newAddr.sin_addr), ntohs(newAddr.sin_port));
          break;
        }
        else
        {
          printf("Client: %s\n", buff);
          send(new_socket, buff, strlen(buff), 0);
          bzero(buff, sizeof(buff));
        }
      }
    }
  }

  close(new_socket);

  return 0;
}
```

**Penjelasan:**

```c
struct table{ ... };

struct permission{ ... };

struct permission_db{ ... };
```

- **struct table** : Struktur ini digunakan untuk merepresentasikan sebuah tabel.

- **struct permission** : Struktur ini digunakan untuk menyimpan informasi izin akses.

- **struct permission_db** : Struktur ini digunakan untuk menyimpan informasi izin akses terkait dengan sebuah database.

```c
int isUserExist(char *uname){ ... }
```

- Fungsi **isUserExist** bertujuan untuk memeriksa keberadaan _user_ dalam sebuah file database.


```c
void createUser(char *name, char *pass){ ... }
```

- Fungsi **createUser** bertujuan untuk membuat entri _user_ baru dalam file database.


```c
int isAllowedDb(char *name, char *database){ ... }
```

- Fungsi ini digunakan untuk memeriksa apakah _user_ dengan nama tertentu memiliki izin akses terhadap database yang ditentukan. Informasi izin akses dibaca dari file database `permission.dat` di dalam direktori `databases/`.

```c
void insertPermission(char *name, char *database){ ... }
```

- Fungsi ini digunakan untuk memasukkan izin akses baru dengan _username_ dan nama database yang diberikan ke dalam file database `permission.dat` di dalam direktori `databases/`.


```c
int findColumn(char *table, char *column) { ... }
```

- Fungsi ini digunakan untuk mencari indeks kolom yang sesuai dengan nama kolom yang diberikan dalam tabel yang disimpan dalam file.


```c
int deleteColumn(char *table, int index){ ... }
```

- Fungsi **deleteColumn** bertujuan untuk menghapus kolom tertentu dari sebuah tabel yang disimpan dalam file.

```c
int deleteTable(char *table, char *namaTable){ ... }
```

- Fungsi **deleteTable** digunakan untuk menghapus tabel dengan menyimpan informasi kolom yang ada sebelumnya ke dalam file sementara, kemudian mengganti nama file sementara menjadi nama file tabel yang dihapus.

```c
int updateColumn(char *table, int index, char *ganti){ ... }
```

- Fungsi **updateColumn** digunakan untuk mengganti nilai data pada kolom tertentu dalam tabel dengan menyimpan perubahan ke dalam file sementara, kemudian mengganti nama file sementara menjadi nama file tabel yang telah diubah.


```c
int updateColumnWhere(char *table, int index, char *ganti, int change_index, char *where){ ... }
```

- Fungsi **updateColumnWhere** berguna untuk melakukan pembaruan data pada kolom tertentu dalam tabel berdasarkan kondisi tertentu.

```c
int deleteTableWhere(char *table, int index, char *column, char *where){ ... }
```

- Fungsi **deleteTableWhere** ini berguna untuk menghapus baris-baris dalam tabel berdasarkan kondisi tertentu, di mana kondisi tersebut ditentukan oleh kolom yang diberikan dan nilai yang harus terpenuhi.

```c
void writelog(char *cmd, char *name){ ... }
```

- Fungsi **writelog** ini berguna untuk mencatat log aktivitas _user_ ke dalam file "logUser.log", dengan mencatat waktu, nama _user_, dan perintah yang dilakukan.

Kemudian lanjut ke bagian `main`. 

```c
int sockfd, ret;
struct sockaddr_in serverAddr;

int new_socket;
struct sockaddr_in newAddr;

socklen_t addr_size;

char buff[1024];
pid_t childpid;
```

- **sockfd** sebagai file descriptor soket.

- **ret** untuk menyimpan nilai kembalian dari beberapa pemanggilan fungsi.

- **sockaddr_in serverAddr** untuk menyimpan informasi tentang alamat server.

- **new_socket** sebagai file descriptor untuk soket baru yang akan digunakan untuk menerima koneksi.

- **sockaddr_in newAddr** untuk menyimpan informasi tentang alamat koneksi yang baru diterima.

- **addr_size** dengan tipe socklen_t untuk menyimpan ukuran dari alamat.

- **buff** dengan ukuran 1024 sebagai buffer untuk membaca dan menulis data pada soket.

- **childpid** dengan tipe pid_t untuk menyimpan ID _child process_.


```c
sockfd = socket(AF_INET, SOCK_STREAM, 0);
if (sockfd < 0){
  printf("[-]Error connection.\n");
  exit(1);
}
printf("[+]Socket created.\n");

memset(&serverAddr, '\0', sizeof(serverAddr));
serverAddr.sin_family = AF_INET;
serverAddr.sin_port = htons(PORT);
serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
```

- Kemudian menyiapkan soket dan mengatur alamat server untuk digunakan dalam langkah-langkah selanjutnya, seperti mengikat soket ke alamat dan mendengarkan koneksi.

```c
ret = bind(sockfd, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
if (ret < 0){
  printf("[-]Error in binding.\n");
  exit(1);
}
printf("[+]Bind to port %d\n", PORT);

if (listen(sockfd, 10) == 0)
  printf("[+]Listening....\n");
else
  printf("[-]Error in binding.\n");
```

- Pemanggilan fungsi **bind()** digunakan untuk mengikat soket ke alamat server.

- Nilai yang di-_return_ oleh **bind()** disimpan dalam variabel `ret`. Jika nilai `ret` kurang dari 0, maka terjadi kesalahan dalam mengikat soket ke alamat.

- Pemanggilan fungsi **listen()** digunakan untuk memulai mendengarkan koneksi dari klien.

- Jika **listen()** me-_return_ nilai yang tidak sama dengan 0, maka terjadi kesalahan dalam memulai mendengarkan koneksi.

Untuk potongan kode selanjutnya hingga selesai, semua berada dalam lingkup _loop_ **while(1){ ... }**

```c
while (token != NULL){
  strcpy(cmd[i], token);
  i++;
  token = strtok(NULL, ":");
}
```

- String awal yang dipecah menjadi beberapa token menggunakan `delimiter` ":" akan disimpan dalam array `cmd[]` untuk penggunaan selanjutnya.


```c
if (strcmp(cmd[0], "cUser") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **cUser** menggunakan fungsi `strcmp`. Jika kondisi terpenuhi, maka dilakukan pengujian lain dengan membandingkan nilai `cmd[3]` dengan string **0**. Jika juga terpenuhi, maka akan memanggil fungsi `createUser` dengan argumen `cmd[1]` dan `cmd[2]`.

- Jika nilai `cmd[3]` bukan **0**, maka akan dikirimkan pesan **Not Allowed: Not have permission**. Lalu variabel `buff` akan dihapus isinya.


```c
else if (strcmp(cmd[0], "gPermission") == 0){ .. }
```

- Cek apakah nilai `cmd[0]` sama dengan **gPermission**. Jika terpenuhi, maka akan dilakukan pengujian lain dengan membandingkan nilai `cmd[3]` dengan string "0".

- Jika kondisi kedua terpenuhi, maka:

1. Pertama, fungsi **isUserExist**.
2. Jika _user_ (`cmd[2]`) ditemukan (`exist` = 1), maka fungsi **insertPermission** akan dipanggil dengan argumen `cmd[2]` dan `cmd[1]` untuk memasukkan izin (`cmd[1]`) kepada _user_ tersebut.
3. Jika _user_ (`cmd[2]`) tidak ditemukan (`exist` != 1), maka variabel `buff` akan dihapus isinya.

- Jika kondisi kedua tidak terpenuhi (`cmd[3]` != "0"), maka variabel `buff` akan dihapus isinya.

```c
else if (strcmp(cmd[0], "cDatabase") == 0){ ... }
```

- Cek apakah nilai `cmd[0]` sama dengan string **cDatabase** menggunakan. Jika terpenuhi.

- Dengan demikian, kondisi **cDatabase** digunakan untuk membuat direktori baru untuk _database_ dan memberikan izin tertentu ke _user_ yang diberikan.


```c
else if (strcmp(cmd[0], "uDatabase") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **uDatabase** menggunakan fungsi `strcmp`.

- Kondisi **uDatabase** digunakan untuk memeriksa izin _user_ untuk mengakses _database_ dan memberikan respons sesuai dengan izin yang diberikan. Jika izin diberikan, _database_ tersebut akan ditandai sebagai _database_ yang sedang digunakan.


```c
else if (strcmp(cmd[0], "cekCurrentDatabase") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **cekCurrentDatabase** menggunakan fungsi strcmp.

- Kondisi **cekCurrentDatabase** digunakan untuk memeriksa _database_ yang sedang digunakan oleh _user_. Jika tidak ada _database_ yang dipilih, pesan yang sesuai akan dikirimkan.

```c
else if (strcmp(cmd[0], "cTable") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **cTable**.

- Kondisi **cTable** digunakan untuk membuat tabel baru dalam _database_ yang sedang digunakan. Tabel tersebut akan memiliki kolom-kolom yang ditentukan oleh query yang diterima.

```c
else if (strcmp(cmd[0], "dDatabase") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **dDatabase**.

- Kondisi **dDatabase** digunakan untuk menghapus _database_. Sebelumnya, izin _user_ akan diperiksa, dan jika _user_ diizinkan, _database_ akan dihapus.

```c
else if (strcmp(cmd[0], "dTable") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **dTable**.

- kondisi **dTable** digunakan untuk menghapus tabel dalam _database_ yang sedang digunakan. Sebelumnya, akan dicek apakah _user_ telah memilih _database_. Jika _database_ telah dipilih, tabel akan dihapus.

```c
else if (strcmp(cmd[0], "dColumn") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **dColumn**.

- Kondisi **dColumn** digunakan untuk menghapus kolom dari tabel dalam _database_ yang sedang digunakan. Terlebih dahulu, akan dicek apakah _user_ telah memilih _database_. Jika _database_ telah dipilih, kolom akan dihapus dari tabel.

```c
else if (strcmp(cmd[0], "insert") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **insert**.

- Kondisi **insert** digunakan untuk memasukkan data ke dalam tabel pada _database_ yang sedang digunakan. Sebelumnya, akan dicek apakah _user_ telah memilih _database_. Jika _database_ telah dipilih, data akan diinsert ke dalam tabel dan pesan konfirmasi akan dikirimkan.

```c
else if (strcmp(cmd[0], "update") == 0){ ... }
```

- Cek apakah nilai yang ada di `cmd[0]` sama dengan string **update**.

- Fungsi ini memiliki beberapa tujuan:

1. Memeriksa apakah sebuah _database_ telah dipilih. Jika tidak, maka akan dikirimkan pesan untuk memberi tahu _user_ bahwa mereka belum memilih _database_.

2. Memisahkan dan menyimpan argumen perintah **update** ke dalam `query_list`.

3. Membentuk _path file_ tabel yang akan diupdate menggunakan `used_db` (_database_ yang dipilih) dan nama tabel yang diperoleh dari `query_list`.

4. Jika jumlah argumen adalah 5, artinya perintah **update** hanya mengubah nilai kolom. Fungsi **findColumn** digunakan untuk menemukan indeks kolom yang sesuai dengan nama kolom yang diberikan. Jika kolom ditemukan, fungsi **updateColumn** akan dipanggil untuk mengubah nilai kolom tersebut.

5. Jika jumlah argumen adalah 8, artinya perintah **update** mengubah nilai kolom dengan mempertimbangkan kondisi. Fungsi **findColumn** digunakan untuk menemukan indeks kolom yang sesuai dengan nama kolom yang diberikan. Jika kedua kolom ditemukan, fungsi **updateColumnWhere** akan dipanggil untuk mengubah nilai kolom tersebut.

6. Jika jumlah argumen tidak sama dengan 5 atau 8, artinya perintah **update** tidak valid.

```c
else if (strcmp(cmd[0], "delete") == 0){ ... }
```

1. Jika sebuah _database_ belum dipilih, maka akan dikirimkan pesan untuk memberi tahu _user_ bahwa mereka belum memilih _database_.

2. Memisahkan dan menyimpan argumen perintah **delete** ke dalam `query_list`.

3. Membentuk _path file_ tabel yang akan dihapus.

4. Jika jumlah argumen adalah 3, maka perintah **delete** hanya menghapus seluruh tabel. Fungsi **deleteTable** dipanggil untuk menghapus tabel tersebut.

5. Jika jumlah argumen adalah 6, maka perintah **delete** menghapus baris-baris yang memenuhi kondisi yang diberikan. Fungsi **findColumn** digunakan untuk menemukan indeks kolom yang sesuai dengan nama kolom yang diberikan. Jika kolom ditemukan, fungsi **deleteTableWhere** akan dipanggil untuk menghapus baris-baris berdasarkan kondisi yang diberikan.

6. Jika jumlah argumen tidak sama dengan 3 atau 6, maka perintah **delete** tidak valid.

```c
else if (strcmp(cmd[0], "select") == 0){...}
```

- Mengambil data dari tabel yang sesuai dengan kondisi yang diberikan.

Berikut adalah isi dari kondisi **select**.

```c
User
if (used_db[0] == '\0'){ ... }
```

- jika _user_ mencoba menjalankan perintah **select** tanpa memilih _database_ terlebih dahulu, mereka akan menerima pesan bahwa belum ada _database_ yang dipilih.

```c
while (toks != NULL){ ... }
```

- Memproses token-token dalam perintah **select**. Setiap token diproses dan disimpan dalam array `query_list` untuk penggunaan selanjutnya.

- Perintah **select** yang diberikan oleh _user_ akan dipisahkan menjadi token-token yang dapat digunakan untuk proses selanjutnya.

```c
if (total == 4){ ... }
```

- Jika jumlah token **total** dalam adalah 4, maka dilakukan beberapa langkah untuk melakukan operasi select pada tabel yang ditentukan.

- Dengan langkah-langkah tersebut, dilakukan operasi **select** pada tabel yang ditentukan. Hasil select dalam bentuk baris-baris data dikirimkan ke socket untuk dikirim ke klien.

```c
else if (total == 7 && strcmp(query_list[4], "WHERE") == 0){ ... }
```

-  Jika jumlah token **total** adalah 7 dan `query_list[4]` adalah **WHERE**, maka dilakukan beberapa langkah tambahan untuk melakukan operasi select dengan kondisi `WHERE` pada tabel yang ditentukan.

- Dengan langkah-langkah tersebut, dilakukan operasi **select** pada tabel yang ditentukan dengan kondisi `WHERE` berdasarkan perintah **select** yang diberikan oleh _user_. Hasil select dalam bentuk baris-baris data yang memenuhi kondisi `WHERE` dikirimkan ke socket untuk dikirim ke klien.

```c
else{ ... }
```

- jika perintah **select** tidak memenuhi kondisi sebelumnya (jumlah token adalah 4 atau 7 dengan token keempat adalah `WHERE`), maka akan dilakukan operasi select tanpa kondisi WHERE pada tabel yang ditentukan.

- Indeks kolom yang ditemukan dapat digunakan untuk pemrosesan lebih lanjut, seperti menampilkan data dari kolom-kolom tersebut.

```c
else if (strcmp(cmd[0], "log") == 0){ ... }
```

- jika perintah yang diberikan adalah **log**, maka dilakukan operasi penulisan log.

- Pesan log akan ditulis ke `file log` yang ditentukan, dan pesan `newline` dikirim sebagai respons ke soket baru.

```c
f (strcmp(buff, ":exit") == 0){
  ...
} else {
  ...
}
```

- Cek jika isi **buff** adalah `:exit` maka:

1. Mencetak pesan `Disconnected from [IP address]:[port number]`, dengan nilai **IP address** dan **port number** yang diperoleh dari **newAddr**.**sin_addr** dan **newAddr.sin_port** menggunakan fungsi **inet_ntoa** dan **ntohs**.

2. Keluar dari loop utama, sehingga koneksi dengan client akan terputus.

- Jika tidak terpenuhi, maka:

1. Mencetak pesan `Client: [isi pesan]`, dengan isi pesan yang diterima dari client terdapat pada buff.

2. Mengirim kembali pesan yang diterima dari client ke **new_socket**.

3. Mereset **buff** untuk menyiapkan buffer untuk menerima perintah selanjutnya dari klien.


### client_dump.c 

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4012

struct permission
{
  char name[10000];
  char pass[10000];
};

int checkPermission(char *uname, char *pass)
{
  FILE *file;
  struct permission user;
  int id, mark = 0;

  file = fopen("../database/databases/user.dat", "rb");

  while (1)
  {
    fread(&user, sizeof(user), 1, file);
    if (strcmp(user.name, uname) == 0)
      if (strcmp(user.pass, pass) == 0)
        mark = 1;
    if (feof(file))
      break;
  }

  fclose(file);

  if (mark == 0)
  {
    printf("Not Allowed: Not have permission\n");
    return 0;
  }
  else
    return 1;
}

int main(int argc, char *argv[])
{
  int allowed = 0;
  int id_user = geteuid();
  char used_db[1000];

  if (geteuid() == 0)
    allowed = 1;
  else
  {
    int id = geteuid();
    allowed = checkPermission(argv[2], argv[4]);
  }

  if (allowed == 0)
    return 0;

  FILE *file;
  char loc[10000];
  snprintf(loc, sizeof loc, "../database/log/log%s.log", argv[2]);
  file = fopen(loc, "rb");
  char buff[20000];
  char cmd[100][10000];
  int mark = 0;

  while (fgets(buff, 20000, file))
  {
    char *token;
    char temp_buff[20000];
    snprintf(temp_buff, sizeof temp_buff, "%s", buff);
    token = strtok(temp_buff, ":");
    int i = 0;

    while (token != NULL)
    {
      strcpy(cmd[i], token);
      i++;
      token = strtok(NULL, ":");
    }

    char *b = strstr(cmd[4], "USE");
    if (b != NULL)
    {
      char *tokens;
      char temp_cmd[20000];
      strcpy(temp_cmd, cmd[4]);
      tokens = strtok(temp_cmd, "; ");
      int j = 0;

      char use_cmd[100][10000];

      while (tokens != NULL)
      {
        strcpy(use_cmd[j], tokens);
        j++;
        tokens = strtok(NULL, "; ");
      }

      char databaseTarget[20000];
      if (strcmp(use_cmd[1], argv[5]) == 0)
        mark = 1;
      else
        mark = 0;
    }
    if (mark == 1)
      printf("%s", buff);
  }
  fclose(file);
}
```

**Penjelasan:**

```c
int checkPermission(char *uname, char *pass){ ... }
```

- Fungsi **checkPermission** mengambil dua argumen: `uname` (username) dan `pass` (password). Fungsi ini bertujuan untuk memeriksa izin atau permission dari _user_.

Kemudian lanjut ke bagian `main`.

```c
if (geteuid() == 0)
  allowed = 1;
else{
  int id = geteuid();
  allowed = checkPermission(argv[2], argv[4]);
}
```

- memeriksa nilai **user** yang sedang menjalankan program adalah **superuser** (dengan user ID 0) atau memerlukan pengecekan izin melalui pemanggilan fungsi **checkPermission**.

```c
while (fgets(buff, 20000, file)){ ... }
```

- Membaca baris-baris teks dari file, memprosesnya, dan mencetak baris-baris yang memenuhi kriteria tertentu. Jika terdapat kata **USE** dalam `cmd[4]` dan `use_cmd[1]` sama dengan `argv[5]`.
