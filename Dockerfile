FROM ubuntu:latest

RUN apt-get update && apt-get install -y gcc

COPY database/database.c .
COPY client/client.c .

RUN gcc database.c -o database

RUN gcc client.c -o client

CMD sh -c './database & ./client'